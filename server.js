// Copyright (C) Andrew Palmer

var pg = require('pg');
var http = require('http');
var https = require('https');
var _ = require('underscore');
var url = require('url');
var qs = require('querystring')

function generate_new_post_htm(parent_timestamp) {
	return '<div class="post"><form action="/new_post" method="post">'
		+ (parent_timestamp ? '<input type="hidden" name="parent_timestamp" value="' + parent_timestamp + '"/>'  : '')
		+ '<input type="text" name="author" value="Anon"/>'
		+ '<input type="submit" value="Post!" style="margin-left: 0.5%; margin-bottom: 0.5%;"/><br>'
		+ '<textarea rows="3" name="body" value="Body of Post"></textarea>'
		+ '</form></div><br>';
}

function generate_title() {
	return '<a href="/" style="text-decoration: none;"><h1 style="color: magenta;">qxczv.pw</h1></a>'+
		'<div><h5>Please follow the <a href="https://www.heroku.com/policy/aup">rules</a></h5>' +
		'<a class="pushbullet-subscribe-widget" data-channel="qxczv" data-widget="button" data-size="small"></a>'+
		"<script type=\"text/javascript\">(function(){var a=document.createElement('script');a.type='text/javascript';a.async=true;a.src='https://widget.pushbullet.com/embed.js';var b=document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a,b);})();</script>" +
		'<input type="text" id="searchquery" style="margin-left: 1%"/> <input type="button" onclick="window.location=\'/?/\'+document.getElementById(\'searchquery\').value" value="Search"/></div><br>';
}

function rmv_quotes(s) {
	//while(s.indexOf('"') > -1) s = s.replace('"', "''");
	return s;
}

function parse_tag(tgs) {
	var tg = tgs.split(' ');
	if(tg == undefined || tg.length == 0 || tg[0] == "") return "";
	if(tg[0] == 'img') {
		return '<img src="' + rmv_quotes(tg[1]) + '"/>';
	} else if(tg[0] == 'link') {
		return '<a href="' + rmv_quotes(tg[1]) + '">' + _.escape(tg[1]) + '</a>';
	} else return '['+tgs+']';
}
function process_user_body(inp) {
	if(inp[0] == '$' && inp[1] == '%' && inp[2] == '$') return inp;
	var oup = '';
	for(var i = 0; i < inp.length; ++i) {
		if(inp[i] == '[') {
			var clb = inp.indexOf(']', i);
			if(clb == -1 || clb == i+1) {
				oup += inp[i];
				continue;
			}
			oup += parse_tag(inp.substr(i+1,clb-1));
			i = clb+1;
		}
		else if(inp[i] == '\n') {
			oup += '<br>';
		}
		else {
			oup += inp[i];
		}
	}
	return oup;
}

var last_pnt = _.now()-600000;
function pushbullet_notification() {
	var nw = _.now();
	if( (nw - last_pnt) > 30000 ) {
		last_pnt = nw;
		var req = https.request({
			host: 'api.pushbullet.com',
			path: '/v2/pushes',
			method: 'POST',
			headers: {
				'Access-Token': process.env.PUSHBULLET_API_KEY,
				'Content-Type': 'application/json'
			}
		}, function(response) {
			var str = ''
			response.on('data', function (chunk) {
			  str += chunk;
			});

			response.on('end', function () {
			  console.log(str);
			});
		});
		var msg = {
			type: 'link',
			title: 'New Posts on qxczv.pw!',
			url: 'http://www.qxczv.pw',
			channel_tag: 'qxczv'
		};
		req.write(JSON.stringify(msg));
		req.end();
	}
}



function generate_post_htm(res, post, client, finish, reclvl) {
	if(!post) { finish(); return; }
	client.query('SELECT * FROM posts WHERE parent_timestamp = $1 ORDER BY timestamp', [post.timestamp],
	function(err, result) {
		if (err) { console.error(err); res.write('<p>Error: ' + JSON.stringify(err) + '</p>'); }
		else {
			var static_link = '/ipost?id='+post.timestamp;
			res.write('<div class="post"><h4><b>' + post.author + '</b> | static: <a href="'+static_link+'">'+static_link+'</a></h4>' + '<p>' + post.body + '</p>');
			var i = 0;
			var cb = function() {
				if(i < result.rowCount) {
					i++;
					generate_post_htm(res, result.rows[i], client, cb, reclvl+1);
				} else {
					if(reclvl < 4) res.write(generate_new_post_htm(post.timestamp));
					res.write('</div><br>');
					finish();
				}
			};
			generate_post_htm(res, result.rows[i], client, cb, reclvl+1);
		}
	});
}

function serve_css(res) {
	res.writeHead(200, {'Content-Type': 'text/css'});
	res.end(".post { border-style: solid; border-width: 2px; padding-left: 8px; padding-right: 8px; padding-top: 8px; margin-left: 3%; }" +
			"textarea { color: green; background-color: black; border-color: darkgreen; width: 99.5%; margin-bottom: 0.5%; }" +
			"input { color: green; background-color: black; border-color: darkgreen; }" +
			"img { width: 100%; }" +
			"div { color: green; } body { background-color: black; }");
}

function handle_new_post(req, res, client) {
	var sd = '';
	req.on('data', function(data) {
		sd += data;
	});
	req.on('end', function() {
		var pd = qs.parse(sd);
		console.log(req.url + ' @ ' + JSON.stringify(pd));
		var pv = [_.now(), _.escape(pd.author), process_user_body(_.escape(pd.body))];
		var qr = 'INSERT INTO posts VALUES ($1, $2, $3';
		if(pd.parent_timestamp) {
			pv[pv.length] = pd.parent_timestamp;
			qr += ', $4';
			client.query('UPDATE posts SET last_reply_timestamp=$1 WHERE timestamp=$2', [pv[0], pd.parent_timestamp], function(err, result) {
				if(err) { console.log(JSON.stringify(error)); throw err; }
			});
		}
		qr += ')';
		client.query(qr, pv, function(err, result) {
			if(err) { console.log(JSON.stringify(err)); throw err; }
			pushbullet_notification();
			res.writeHead(303, {"Location":'/'});
			res.end("redirect");
		});
	});
}

function serve_page(req, res, client) {
	var cquery = 'SELECT * FROM posts where parent_timestamp is null '
	var cquery_end = ' ORDER BY timestamp DESC LIMIT 25';
	var offset = 0;
	if(req.url != '/') {
		var urlbits = req.url.split('/');
		var next_bit = 1;
		if(urlbits[next_bit] == '?') {
			next_bit++;
			var qry = _.escape(urlbits[next_bit++]);
			//cquery += "WHERE body LIKE '%" + qry + "%' OR author LIKE '%"+qry+"%' ";
		}
		if(next_bit < urlbits.length) {
			offset = parseInt(urlbits[next_bit]);
			if(!isNaN(offset)) cquery_end += ' OFFSET ' + offset;
		}
	}

	client.query(cquery+cquery_end, function(err, result) {
		res.writeHead(200, {'Content-Type': 'text/html'});
		if (err) { console.error(err); res.end('<html>Error: ' + JSON.stringify(err) + '</html>'); }
		else {
			res.write('<!DOCTYPE html><html><head><title>qxczv</title><link rel="stylesheet" href="/styles.css"></head><body>');
			res.write(generate_title());
			res.write(generate_new_post_htm());
			var i = 0;
			var cb = function() {
				if(i < result.rowCount) {
					i++;
					generate_post_htm(res, result.rows[i], client, cb, 0);
				} else {
					res.write(	'<p style="text-align: center;"><a href="/'+Math.max(0,offset-25)+'">last page</a> ~~<a href="/0">front page</a>~~ '+
								'<a href="/'+(offset+25)+'">next page</a></p>'+
								'</body></html>');
					res.end();
				}
			};
			generate_post_htm(res, result.rows[i], client, cb, 0);
		}
	});
}

function run_server(req, res, client) {
	var rqurl = url.parse(req.url, true);
	if(rqurl.pathname == '/styles.css') {
		serve_css(res);
	} else if(rqurl.pathname == '/new_post' && req.method == 'POST') {
		handle_new_post(req, res, client);
	} else if(rqurl.pathname == '/ipost') {
		res.write('<!DOCTYPE html><html><head><title>qxczv</title><link rel="stylesheet" href="/styles.css"></head><body>');
		res.write(generate_title());
		client.query('SELECT * from posts where timestamp='+parseInt(rqurl.query.id), function(err, result) {
			if(err) { console.error(err); res.end('<html>Error</html>'); return; }
			generate_post_htm(res, result.rows[0], client, function() {
				res.write('</body></html>');
				res.end();
			}, 0);
		});
	} else {
		serve_page(req, res, client);
	}
}

http.createServer(function (req, res) {
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		if(err) { console.log(JSON.stringify(err)); throw err; }
		run_server(req, res, client);
		done();
	});
}).listen(process.env.PORT);

console.log('Server running @ '+process.env.PORT);
